package br.com.alura.forum.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession httpSession = request.getSession();

		String uriAcessada = request.getRequestURI();
		boolean uriAcessadaEstaLiberada = urlLiberadas().stream().anyMatch(url -> uriAcessada.contains(url));
		boolean uriAcessadaEhHome = uriAcessada.endsWith(request.getContextPath() + "/");
		boolean usuarioEstaLogado = httpSession.getAttribute("usuarioLogado") != null;
		
		if (uriAcessadaEhHome || uriAcessadaEstaLiberada || usuarioEstaLogado) {
			return true;
		} else {
			response.sendRedirect(request.getContextPath() +"/login");
			return false;
		}
	}
	
	private Set<String> urlLiberadas() {
		Set<String> urls = new HashSet<>();
		urls.addAll(Arrays.asList("/login", "/usuarios", "/topicos", "/static"));
		return urls;
	}

}
